package chess

import "errors"

func CanKnightAttack(white, black string) (bool, error) {
	whitePos := []rune(white)
	blackPos := []rune(black)

	if len(white) != 2 || len(black) != 2 {
		return false, errors.New("invalid pos")
	}

	isWhiteValid := whitePos[0] >= 'a' && whitePos[0] <= 'h' && whitePos[1] >= '1' && whitePos[1] <= '8'
	isBlackValid := blackPos[0] >= 'a' && blackPos[0] <= 'h' && blackPos[1] >= '1' && blackPos[1] <= '8'
	if white == black || !isWhiteValid || !isBlackValid {
		return false, errors.New("wrong pos")
	}

	for _, v := range [2][2]int32{{1, 2}, {2, 1}} {
		for _, x := range []int32{-1, 1} {
			for _, y := range []int32{-1, 1} {
				if whitePos[0]+v[0]*x == blackPos[0] && whitePos[1]+v[1]*y == blackPos[1] {
					return true, nil
				}
			}
		}
	}

	return false, nil
}
