package lastlettergame

type g struct {
	Used        map[string]bool
	Transforms  map[rune][]string
	LongestPath []string
}

func (graph *g) dfs(k string, path []string) []string {
	for _, v := range graph.Transforms[[]rune(k)[len(k)-1]] {
		if (graph.Used)[v] {
			continue
		}
		(graph.Used)[v] = true

		newPath := append(path, v)
		newPath = graph.dfs(v, newPath)

		(graph.Used)[v] = false
		if len(newPath) > len(graph.LongestPath) {
			graph.LongestPath = append(make([]string, 0), newPath...)
		}
	}

	return path
}

func Sequence(dic []string) []string {
	g := g{make(map[string]bool), make(map[rune][]string), make([]string, 0)}

	for _, v := range dic {
		letters := []rune(v)
		g.Transforms[letters[0]] = append(g.Transforms[letters[0]], v)
	}

	for _, v := range dic {
		g.Used[v] = true
		g.dfs(v, []string{v})
		g.Used[v] = false
	}

	return g.LongestPath
}
