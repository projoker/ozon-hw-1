package secretmessage

import "sort"

// Decode func
func Decode(encoded string) string {
	counts := make(map[rune]int, 27)
	for _, v := range []rune(encoded) {
		counts[v]++
	}

	chars := make([]rune, 0)
	for k, v := range counts {
		if v > counts['_'] {
			chars = append(chars, k)
		}
	}

	sort.Slice(chars, func(i int, j int) bool {
		return counts[chars[i]] > counts[chars[j]]
	})

	return string(chars)
}
