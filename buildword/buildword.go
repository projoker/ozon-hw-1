package buildword

import "math"

func FindPart(pos int, end int, size int, parts map[int][]int) int {
	if pos == end {
		return size
	}

	minSize := math.MaxInt32
	for _, v := range parts[pos] {
		curSize := FindPart(pos+v, end, size+1, parts)
		if curSize < minSize {
			minSize = curSize
		}
	}

	return minSize
}

func BuildWord(word string, fragments []string) int {
	parts := make(map[int][]int, 0)

	for _, fragment := range fragments {
		for i := 0; i < len(word)-len(fragment)+1; i++ {
			if word[i:i+len(fragment)] == fragment {
				parts[i] = append(parts[i], len(fragment))
			}
		}
	}

	size := FindPart(0, len(word), 0, parts)
	if size == math.MaxInt32 {
		return 0
	}

	return size
}
