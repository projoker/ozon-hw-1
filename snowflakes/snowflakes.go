package snowflakes

func OverlaidTriangles(n, m int) int {
	dp := make([]int, n+1)
	dp[0] = 1

	for i := 0; i < n-1; i++ {
		newDp := make([]int, n+1)
		for j := 0; j <= i; j++ {
			for k := 0; k < 3; k++ {
				if j%2 == 0 {
					if k != 2 {
						newDp[j-j%2+k] += dp[j] * 6
					}
				} else {
					if k == 0 {
						newDp[j-j%2+k] -= dp[j]
					} else {
						newDp[j-j%2+k] += dp[j]
					}
				}
			}
		}

		dp = newDp
	}

	return dp[m-1]
}
