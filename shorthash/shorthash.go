package shorthash

import (
	"strings"
)

func GenerateShortHashes(dictionary string, l int) []string {
	if l == 0 {
		return []string{}
	}

	chars := strings.Split(dictionary, "")
	hashes := strings.Split(dictionary, "")

	left := 0
	for i := 2; i <= l; i++ {
		right := len(hashes)
		for _, char := range chars {
			for j := range hashes[left:right] {
				hashes = append(hashes, hashes[left+j]+char)
			}
		}
		left = right
	}

	return hashes
}
