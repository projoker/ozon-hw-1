package brokennode

func FindBrokenNodes(brokenNodes int, reports []bool) string {
	answer := make([]rune, len(reports))
	nodes := 0

	hasPerm := true
	for hasPerm {
		for !isAnswer(nodes, brokenNodes, reports) && hasPerm {
			hasPerm = nextPerm(&nodes, len(reports))
		}

		if !hasPerm {
			break
		}

		addAnswer(answer, nodes)

		hasPerm = nextPerm(&nodes, len(reports))
	}

	return string(answer)
}

func addAnswer(answer []rune, nodes int) {
	status := [2]rune{'B', 'W'}
	for i := range answer {
		if answer[i] == 0 {
			answer[i] = status[nodes&1]
		} else if answer[i] != status[nodes&1] {
			answer[i] = '?'
		}

		nodes = nodes >> 1
	}
}

func isAnswer(nodes int, brokenNodes int, reports []bool) bool {
	curBrokenNodes := 0

	curNode := nodes
	for range reports {
		if curNode&1 == 0 {
			curBrokenNodes++
		}
		curNode = curNode >> 1
	}

	if curBrokenNodes != brokenNodes {
		return false
	}

	last := nodes & 1
	for i := range reports {
		if nodes&1 == 0 {
			nodes = nodes >> 1
			continue
		}

		var ans bool
		if i == len(reports)-1 {
			ans = last == 1
		} else {
			ans = nodes&0b10>>1 == 1
		}

		if ans != reports[i] {
			return false
		}
		nodes = nodes >> 1
	}

	return true
}

func nextPerm(nodes *int, max int) bool {
	*nodes++

	return *nodes != 1<<max
}
