package warriors

import "math"

const (
	NO = iota
	PREV
	CUR
)

func Count(image string) int {
	field := make([][]int, 1)
	field[0] = make([]int, 0)

	for _, v := range []rune(image) {
		switch v {
		case '0':
			field[len(field)-1] = append(field[len(field)-1], 0)
		case '1':
			field[len(field)-1] = append(field[len(field)-1], 1)
		case '\n':
			field = append(field, make([]int, 0))
		}
	}

	row := make([]int, len(field[0]))
	warriors := 0
	for i := 0; i < len(field)+1; i++ {
		warrior := NO
		var prev int
		for j := 0; j < len(row); j++ {
			if i < len(field) && field[i][j] == 1 {
				row[j] = i + 1
			}

			if row[j] == i+1 {
				warrior = CUR
			} else if row[j] == i || warrior != NO && row[j] != 0 && math.Abs(float64(row[j]-prev)) <= 1 {
				if warrior != CUR {
					warrior = PREV
				}
			} else {
				if warrior == PREV {
					warriors++
				}
				warrior = NO
			}

			prev = row[j]
		}

		if warrior == PREV {
			warriors++
		}
	}

	return warriors
}
