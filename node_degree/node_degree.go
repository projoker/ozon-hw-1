package nodedegree

import (
	"errors"
	"fmt"
)

// Degree func
func Degree(nodes int, graph [][2]int, node int) (int, error) {
	if node > nodes {
		err := fmt.Sprintf("node %v not found in the graph", node)
		return 0, errors.New(err)
	}

	degree := 0

	for _, v := range graph {
		if v[0] == node || v[1] == node {
			degree++
		}
	}

	return degree, nil
}
