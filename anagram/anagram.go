package anagram

import (
	"strings"
)

func FindAnagrams(dictionary []string, word string) (result []string) {
	word = strings.ToLower(word)
	letters1 := make(map[rune]int)
	for _, r := range []rune(word) {
		if r >= 'a' && r <= 'z' {
			letters1[r]++
		}
	}

	if len(letters1) == 0 {
		return []string{}
	}

	for i, w := range dictionary {
		w = strings.ToLower(w)
		letters2 := make(map[rune]int)
		for _, r := range []rune(w) {
			if r >= 'a' && r <= 'z' {
				letters2[r]++
			}
		}

		if isEqual(letters1, letters2) && word != w {
			result = append(result, dictionary[i])
		}
	}

	return result
}

func isEqual(left map[rune]int, right map[rune]int) bool {
	if len(left) != len(right) {
		return false
	}

	for k, v := range left {
		if v != right[k] {
			return false
		}
	}

	return true
}
