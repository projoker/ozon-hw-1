package sumdecimal

import (
	"bytes"
	"math/big"
)

func SumDecimal(c int) int {
	if c <= 1 {
		return 0
	}

	sqr := new(big.Float).SetPrec(10000).SetInt64(int64(c))
	sqr.Sqrt(sqr)

	digits := make([]byte, 0)
	digits = sqr.Append(digits, 'f', 1001)
	digits = digits[bytes.IndexByte(digits, '.')+1 : len(digits)-1]

	sum := 0
	for _, v := range digits {
		sum += int(v - '0')
	}

	return sum
}
