package coins

func Piles(n int) int {
	dp := make([][]int, n+1)

	for i := range dp {
		dp[i] = make([]int, n+1)
		dp[i][0] = 0
	}
	dp[0][0] = 1

	for i := 0; i <= n; i++ {
		for j := 1; j <= n; j++ {
			if i >= j {
				dp[i][j] = dp[i][j-1] + dp[i-j][j]
			} else {
				dp[i][j] = dp[i][i]
			}
		}
	}

	return dp[n][n]
}
