package mergesort

// MergeSort is used to sort an array of integer
func MergeSort(input []int) []int {
	if len(input) == 0 {
		return input
	}

	list := make([][]int, len(input))
	for i := range list {
		list[i] = make([]int, 1)
		list[i][0] = input[i]
	}

	for len(list) > 1 {
		for i := 0; i < len(list)/2; i++ {
			newList := make([]int, 0)
			for j, k := 0, 0; j < len(list[i]) || k < len(list[len(list)-1-i]); {
				if k >= len(list[len(list)-1-i]) || j < len(list[i]) && list[i][j] <= list[len(list)-1-i][k] {
					newList = append(newList, list[i][j])
					j++
				} else {
					newList = append(newList, list[len(list)-1-i][k])
					k++
				}
			}
			list[i] = newList
		}
		list = list[:(len(list)+1)/2]
	}

	return list[0]
}
