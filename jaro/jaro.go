package jaro

import (
	"math"
)

func Distance(word1 string, word2 string) float64 {
	runes1, runes2 := []rune(word1), []rune(word2)
	letters1, letters2 := make(map[rune][]int), make(map[rune][]int)

	for i, v := range []rune(word1) {
		if _, ok := letters1[v]; !ok {
			letters1[v] = make([]int, 0)
		}
		letters1[v] = append(letters1[v], i)
	}

	for i, v := range []rune(word2) {
		if _, ok := letters2[v]; !ok {
			letters2[v] = make([]int, 0)
		}
		letters2[v] = append(letters2[v], i)
	}

	s1, s2 := float64(len(runes1)), float64(len(runes2))
	dist := int(math.Max(s1, s2)/2) - 1
	m := 0.0
	letters1cl, letters2cl := make(map[rune][]int), make(map[rune][]int)

	for k := range letters1 {
		for i, j := 0, 0; i < len(letters1[k]) && j < len(letters2[k]); {
			if int(math.Abs(float64(letters1[k][i]-letters2[k][j]))) <= dist {
				if _, ok := letters1cl[k]; !ok {
					letters1cl[k] = make([]int, 0)
				}
				letters1cl[k] = append(letters1cl[k], letters1[k][i])

				if _, ok := letters2cl[k]; !ok {
					letters2cl[k] = make([]int, 0)
				}
				letters2cl[k] = append(letters2cl[k], letters2[k][j])

				m++
				i++
				j++
			} else if letters1[k][i] < letters2[k][j] {
				i++
			} else {
				j++
			}
		}
	}

	t := 0.0
	for i, j := 0, 0; i < len(runes1) && j < len(runes2); {
		if len(letters1cl[runes1[i]]) <= 0 {
			i++
		} else if len(letters2cl[runes2[j]]) <= 0 {
			j++
		} else {
			if letters1cl[runes1[i]][0] != i {
				i++
				continue
			}
			if letters2cl[runes2[j]][0] != j {
				j++
				continue
			}
			if runes1[i] != runes2[j] {
				t++
			}
			letters1cl[runes1[i]] = letters1cl[runes1[i]][1:]
			letters2cl[runes2[j]] = letters2cl[runes2[j]][1:]
			i++
			j++
		}
	}
	t = math.Floor(t / 2)

	return 1.0 / 3 * (m/s1 + m/s2 + (m-t)/m)
}
