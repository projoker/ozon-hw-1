package reverseparentheses

func Reverse(s string) string {
	runes := []rune(s)
	var result string

	opened := 0
	left := -1
	right := -1

	for i, v := range runes {
		if v == '(' {
			opened++
			if left == -1 {
				left = i
			}
		} else if v == ')' {
			opened--
			if opened == 0 {
				result += string(runes[right+1 : left])
				right = i
				mid := []rune(Reverse(string(runes[left+1 : right])))
				for j := 0; j < len(mid)/2; j++ {
					mid[j], mid[len(mid)-1-j] = mid[len(mid)-1-j], mid[j]
				}
				result += string(mid)
				left = -1
			}
		}
	}

	end := runes[right+1:]
	result += string(end)

	return result
}
