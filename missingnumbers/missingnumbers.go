package missingnumbers

import "sort"

func Missing(numbers []int) []int {
	sort.Ints(numbers)

	prev := 0
	result := make([]int, 0)
	var v int
	for _, v = range numbers {
		if v-prev > 1 {
			result = append(result, v-1)
		}
		prev = v
	}

	for len(result) < 2 {
		v++
		result = append(result, v)
	}

	return result
}
