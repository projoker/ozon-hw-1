package functionfrequency

import (
	"go/ast"
	"go/parser"
	"go/token"
	"sort"
)

func FunctionFrequency(gocode []byte) []string {
	fset := token.NewFileSet()
	f, _ := parser.ParseFile(fset, "", gocode, 0)

	stats := make(map[string]int)
	ast.Inspect(f, func(node ast.Node) bool {
		call, ok := node.(*ast.CallExpr)
		if ok {
			switch fun := call.Fun.(type) {
			case *ast.Ident:
				stats[fun.Name]++
			case *ast.SelectorExpr:
				pkg, ok := fun.X.(*ast.Ident)
				if ok {
					stats[pkg.Name+"."+fun.Sel.Name]++
				}
			}
		}

		return true
	})

	rating := make([]string, 0)
	for v := range stats {
		rating = append(rating, v)
	}

	sort.Slice(rating, func(i int, j int) bool {
		return stats[rating[i]] > stats[rating[j]]
	})

	return rating[:3]
}
