package floyd

// Triangle makes a Floyd's triangle matrix with rows count.
func Triangle(rows int) [][]int {
	m := make([][]int, 0)

	for row, i := 0, 1; row < rows; row++ {
		m = append(m, make([]int, 0))
		for len(m[row]) < row+1 {
			m[row] = append(m[row], i)
			i++
		}
	}

	return m
}
