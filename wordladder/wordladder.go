package wordladder

type node struct {
	used bool
	dist int
}

func WordLadder(from string, to string, dic []string) int {
	transforms := make(map[string][]string)
	for _, v := range dic {
		letters := []rune(v)
		for i := range letters {
			key := string(letters[:i]) + "?" + string(letters[i+1:])
			transforms[key] = append(transforms[key], v)
		}
	}

	queue := []string{from}
	g := map[string]node{from: {true, 0}}
	for len(queue) != 0 {
		cur := queue[0]
		queue = queue[1:]

		letters := []rune(cur)
		for i := range letters {
			for _, v := range transforms[string(letters[:i])+"?"+string(letters[i+1:])] {
				if v == to {
					return g[cur].dist + 2
				}

				if !g[v].used {
					queue = append(queue, v)
					g[v] = node{true, g[cur].dist + 1}
				}
			}
		}
	}

	return 0
}
