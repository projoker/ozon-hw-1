package romannumerals

var romansK = []rune{'M', 'D', 'C', 'L', 'X', 'V', 'I'}

var romansV = map[rune]int{
	romansK[0]: 1000, romansK[1]: 500, romansK[2]: 100, romansK[3]: 50, romansK[4]: 10, romansK[5]: 5, romansK[6]: 1,
}

func Encode(n int) (string, bool) {
	if n <= 0 {
		return "", false
	}

	var result []rune

	for i, gr := range romansK {
		for n >= romansV[gr] {
			result = append(result, gr)
			n -= romansV[gr]
		}

		if i == len(romansK)-1 {
			break
		}

		lr := romansK[i+2-i%2]
		if n >= romansV[gr]-romansV[lr] {
			result = append(result, lr, gr)
			n -= romansV[gr] - romansV[lr]
		}
	}

	return string(result), true
}

func Decode(s string) (int, bool) {
	romans := []rune(s)
	var n int

	for i, gr := range romans {
		if _, ok := romansV[gr]; !ok {
			return 0, false
		}

		if i < len(romans)-1 {
			lr := romans[i+1]
			if romansV[gr] < romansV[lr] {
				n += romansV[lr] - romansV[gr]
			} else {
				n += romansV[gr]
			}
		} else if len(romans) == 1 || romansV[romans[i-1]] >= romansV[gr] {
			n += romansV[gr]
		}
	}

	return n, n != 0
}
